/* eslint-disable */
import Vue from 'vue'
import Vuex from 'vuex'

import cart from './modules/cart'
import products from './modules/products'

Vue.use(Vuex)

// eslint-disable-next-line no-new
export default new Vuex.Store({
		modules: {
			cart,
			products
		},

    state: { // = data
     
        
    },
    getters: { // = computed
       
    },
    actions: { // = methods
        
        
    },
    
    mutations: {
       

    }
    
})